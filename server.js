var app = require('./controller/app.js');

var server = app.listen(30001, function()
{
    var port = server.address().port;
    console.log('Web app hosted at http://localhost:%s',port);
});