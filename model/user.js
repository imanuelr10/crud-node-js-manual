var db = require('../databaseConfig.js');
var userDB = {
    //membuat function getProduk
    getProduk: function (id, callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                var sql = 'SELECT * FROM produk_salon WHERE id = ?';
                conn.query(sql, [id], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        return callback(null, result);
                    }
                });
            }
        });
    },

    getProduks: function (callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                var sql = 'SELECT * FROM produk_salon';
                conn.query(sql, function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        return callback(null, result);
                    }
                });
            }
        });
    },
    //jangan lupa berikan tanda koma untuk membuat fungsi baru dibawahnya

    addProduk: function(nama_produk, kategori, jumlah, harga,callback){
        var conn = db.getConnection();
        conn.connect(function (err) {
            if(err){
                console.log(err);
                return callback(err,null);
            }
            else
            {
                console.log("Connected");
                var sql = 'INSERT INTO produk_salon(nama_produk,kategori,jumlah,harga) VALUES (?,?,?,?)';

                conn.query(sql, [nama_produk,kategori,jumlah,harga], function (err,result) {
                    conn.end();
                    if(err)
                    {
                        console.log(err);
                        return callback(err,null);
                    }
                    else
                    {
                        console.log(result.affectedRows);
                        return callback(null, result.affectedRows);
                    }
                });
            }
        });
    },

    updateProduk: function (nama_produk,kategori,jumlah,harga,id,callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if(err){
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                var sql = 'UPDATE produk_salon SET nama_produk=?, kategori=?, jumlah=?,harga=? WHERE id=?';
                conn.query(sql, [nama_produk, kategori, jumlah, harga,id], function (err,result) {
                    conn.end();
                    if(err){
                        console.log(err);
                        return callback(err,null);
                    }
                    else
                    {
                        console.log(result.affectedRows);
                        return callback(null, result.affectedRows);
                    }
                });
            }
        });
    },

    deleteProduk: function(id, callback){
        var conn = db.getConnection();
        conn.connect(function (err) {
            if(err){
                console.log(err);
                return callback(err,null);
            }else
            {
                console.log("Connected!");

                var sql = 'DELETE FROM produk_salon where id=?';

                conn.query(sql,[id],function (err,result) {
                    conn.end();
                    if(err){
                        console.log(err);
                        return callback(err,null);
                    }
                    else
                    {
                        return callback(null,result.affectedRows);
                    }
                });
            }
        });
    }
};
module.exports = userDB;