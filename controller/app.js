//BUAT ROUTING
var express = require('express');
var app = express();
var user = require('../model/user.js');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.get('/api/salon/:produk_id', function (req, res) {
   var id = req.params.produk_id;

   user.getProduk(id,function (err,result) {
       if(!err){
           res.send(result);
       }
       else
       {
           res.status(500).send("Some error");
       }
   });
});

app.get('/api/test/produks', function (req,res) {
    user.getProduks(function (err,result) {
        if(!err){
            res.send(result);
        }
        else
        {
            res.status(500).send("Some error");
        }
    });
});

app.post('/api/produk', urlencodedParser, function (req, res) {

    var nama_produk = req.body.nama_produk;
    var kategori = req.body.kategori;
    var jumlah = req.body.jumlah;
    var harga = req.body.harga;

    user.addProduk(nama_produk, kategori, jumlah, harga, function (err, result) {
        if (!err) {
            console.log(result);
            res.send(result + ' record inserted');
        } else{
            res.send(err.statusCode);

        }
    });

});

app.post('/api/update/:id',urlencodedParser, function (req,res) {
    var id = req.params.id;
    var nama_produk = req.body.nama_produk;
    var kategori = req.body.kategori;
    var jumlah = req.body.jumlah;
    var harga = req.body.harga;

    user.updateProduk(nama_produk,kategori,jumlah,harga,id, function (err,result) {
        if(!err){
            console.log(result);
            res.send(result+' record updated');
        }else
        {
            res.send(err.statusCode);
        }
    });
});

app.delete('/api/delete/:id',function (req,res) {
    var id = req.params.id;

    user.deleteProduk(id,function (err,result) {
        if(!err){
            res.send(result+'record deleted');
        }
        else {
            console.log(err);
            res.status(500).send("Some error");
        }
    });
});


module.exports = app